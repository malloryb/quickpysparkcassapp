from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext
from pyspark_cassandra import CassandraSparkContext,Row
from datetime import datetime, date, time, timedelta
import sys


def main():
    conf = SparkConf().setAppName("PySpark Cassandra Test").set("spark.cassandra.connection.host", "cass.trendkite.com")

    sc = CassandraSparkContext(conf=conf)
    sc = SQLContext(sc)

    rdd = sc.read \
        .format("org.apache.spark.sql.cassandra") \
        .options(keyspace="tkprod", table="ga_data_raw") \
        .load().cache()

    #accounts = sc.createDataFrame(rdd)
    #
    # rdd.registerTempTable("accounts")
    # print("CREATED TEMP TABLE")
    # ga_accounts = sc.sql("SELECT * FROM accounts")

    #ga_accounts = sc.sql("SELECT ga_account_id, date, full_referrer FROM accounts")
                         #"#WHERE ga_account_id='67588728' AND date='2016-02-07 18:00:00'").explain()
    #ga_accounts = ga_accounts.rdd.map.toDF()#.map(lambda p: (p.ga_account_id, )).toDF()
    #ga_accounts.write.parquet("test.parquet", mode="overwrite")
    #ga_accounts.write.parquet("s3://cass-pyspark-test/testPySparkParquet/prod.parquet", mode="overwrite")

    #for acct in ga_accounts.collect():
    #    print(acct.unique())
    #rdd.select("ga_account_id", "date", "full_referrer").where("ga_account_id=?" ,"67588728").where("date=?",'2016-02-07 18:00:00').explain()


    #ids = [(arg[1], str(arg[2] + " " + arg[3])), (arg[4], str(arg[5] + " " + arg[6])), (arg[7], str(arg[8] + " " + arg[9]))]

    # cass = sc.cassandraTable("tkdev", "ga_data_raw").select("ga_account_id","date", "full_referrer", "bounces","goal_completions_all",
    #                                                             "goal_value_all","medium","new_users","pageviews","query_date","sampled",
    #                                                             "session_duration","sessions","social_network","social_source_referral",
    #                                                             "source","transaction_revenue")\
    #     .where("ga_account_id=?", "67588728").where("date=?", '2016-02-07 18:00:00').collect()
        # .where("ga_account_id = ?", "41060023")\
    #     # .where("date=?", [(((datetime.utcnow() - timedelta(days=x)).date(), ))])#.date()))])
    #     # rows.append(cass)
    #     # print(cass.first())

    #.where("date = ?", Date.from(LocalDate.of(2014, 1, 1).atStartOfDay(ZoneOffset.UTC).toInstant))#.combineByKey(lambda a, b: a + b)



   # print(rows.count())
    #cass.saveToCassandra("tkdev", "testspark").columns(["ga_account_id", "date"])
    #cass.map.toDF()

    rdd.write.parquet("s3://cass-pyspark-test/testPySparkParquet/prod.parquet", mode="overwrite")

    #rdd.write.parquet("test.parquet", mode="overwrite")

    #print(cass.collect())



if __name__ == "__main__":
    main()
