#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name='pysparkCassQuery',
    version='0.1.0',
    description='App to query against Cassandra using PySpark',
    long_description="",
    #url='https://github.com/anguenot/pyspark-cassandra',
    license='Apache License 2.0',
    packages=find_packages(),
    include_package_data=True,
)
